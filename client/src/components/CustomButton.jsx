import React, { useState } from 'react';

const CustomButton = ({name, funcion}) => {

  return (
  <button
  onClick={() => {
  funcion()
  }}
  >
    {name}
  </button>
  )
};

export default CustomButton;
